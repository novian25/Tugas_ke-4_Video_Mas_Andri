<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Contoh Project - @yield('content')</title>
</head>
<body>
	<h1>Header Contoh Project</h1>

	@yield('content')
	
	<h1>footer Contoh Project</h1>	
</body>
</html>
