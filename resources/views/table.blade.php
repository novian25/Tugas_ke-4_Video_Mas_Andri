<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ini Table</title>
</head>
<body>
	<table border="1">
		<tr>
			<td>Nama</td>
			<td>Alamat</td>
			<td>email</td>
		</tr>
		@foreach($mahasiswa as $data)
		<tr>
			<td>{{ $data->nama }}</td>
			<td>{{ $data->alamat }}</td>
			<td>{{ $data->email }}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>
