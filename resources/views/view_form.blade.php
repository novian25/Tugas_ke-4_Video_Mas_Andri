@extends('layouts.layout')

@section('title')
	Input Form
@endsection

@section('content')

	<form action="/input_form_post" method="POST">
		<table >
			<tr>
				<td>Nama</td>
				<td>{{ $nama}}</td>
			</tr>

			<tr>
				<td>Alamat</td>
				<td>{{ $alamat}}</td>
			</tr>

			<tr>
				<td>No Telp</td>
				<td>{{ $no_telp}}</td>
			</tr>

			

			
		</table>
	</form>

@endsection
