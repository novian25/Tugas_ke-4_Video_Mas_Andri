<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/profile', function () {
    return ('Ini Adalah Page Profile');
});

// Route::get('/profile_view', function () {
//     return view('page_profile');
// });


Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        return('Ini Page Admin/ User');
    });

});

Route::get('/contact', function () {
    return view('templates.contact');
});


Route::get('/content/{id}','ContohController@tampilkan');

Route::get('/input_form','ContohController@input');

Route::get('/table','ContohController@table');
