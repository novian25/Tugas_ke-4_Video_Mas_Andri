<?php

namespace App\Http\Controllers;

use App\mahasiswa;
use Illuminate\Http\Request;

class ContohController extends Controller
{
     function tampilkan($nama)
    {
    	 return view('content')
    	->with('var_nama',$nama);

    }

     function input()
    {
    	return view('input_form');
    }

    function input_post(Request $request)
    {
    	$nama = $request->nama;
    	$alamat = $request->alamat;
    	$no_telp = $request->no_telp;

    	return view('view_form')
    	->with('nama',$nama)
    	->with('alamat',$alamat)
    	->with('no_telp',$no_telp);
    }


    function table()
    {
    	$mahasiswa = mahasiswa::where('id','>=','2')->get(); 

    	return view('table')
    	->with('mahasiswa',$mahasiswa);
    }


}
